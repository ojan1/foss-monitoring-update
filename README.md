# foss-monitoring-update

[![Build Status](https://cicd.iojo.net/api/badges/ojan/foss-monitoring-update/status.svg)](https://cicd.iojo.net/ojan/foss-monitoring-update)

Requires:

- Jekyll
- Feed2json
- YQ
- JQ

## Drone

Pour pouvoir monter un volume dans un runner drone, il faut que le repo soit certifié au niveau Drone. Pour ce faire, il faut créer un utilisateur admin au boot du conteneur docker avec cette ligne dans la commande docker.

```bash
--env=DRONE_USER_CREATE=username:ojan,admin:true,machine:false
```

