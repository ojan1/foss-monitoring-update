---
layout: page
title: A propos
permalink: /about/
---

Ce site recense les dernières versions des principaux logiciels Open Source dans le domaine du monitoring.

Il est édité par [monitoring-fr.org](https://www.monitoring-fr.org)

## English

A simple website that tracks upgrades of major Open Source monitoring tools.

Brought to you by [monitoring-fr.org](https://www.monitoring-fr.org) and Olivier Jan.
