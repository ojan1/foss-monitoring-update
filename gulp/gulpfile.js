const htmlmin = require('gulp-htmlmin');
const { src, dest } = require('gulp');

exports.default = function() {
  return src('../_site/**/*.html')
    .pipe(htmlmin())
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(dest('../_site/'));
}

