#!/usr/bin/env bash

# dependencies:
# - `yq` and `jq` in `$PATH`
# - [feed2json](https://github.com/appsattic/feed2json.org) for atom2json feed transformation

##################################################
# Bash script to find which OS
##################################################

case "$(uname)" in
   Darwin)
     echo 'Mac OS X'
     sed_binary=gsed
     which ${sed_binary}
     ;;
   Linux)
     echo 'Linux'
     sed_binary=sed
     which ${sed_binary}
     ;;
esac

##################################################
# Assign vars depending on environment
##################################################

for ARGUMENT in "$@"
do
    env=$(echo $ARGUMENT | cut -f2 -d=)   
done

if [ ${env} == dev ]; then
    feed2json_url="localhost:3000"
elif [ ${env} == prod ]; then
    feed2json_url="10.1.1.65:3000"
fi

relative_path="./content/_github"

##################################################
# Main loop
##################################################

for software in $(cat ./soft.txt)
do
    name=$(echo ${software} | cut -d '/' -f5 | tr '[:upper:]' '[:lower:]')
    echo ${name}

    # get and transform atom feeds in json feeds
    curl "http://${feed2json_url}/convert?url=${software}/tags.atom" > ./_data/github/tags/${name}.json
    curl "http://${feed2json_url}/convert?url=${software}/releases.atom" > ./_data/github/releases/${name}.json

    # assign recorded and feeded versions to vars
    latest_version_date=$(cat ./_data/github/tags/${name}.json | jq '.items[0].date_published')
    recorded_version_date=$(yq r ${relative_path}/${name}.md latest_release_date)

    # if markdown file not exist, we create and populate it with values from the feed
    if [ ! -f ${relative_path}/${name}.md ]; then
        echo "---" > ${relative_path}/${name}.md
        echo "title: Tags from ${name}" >> ${relative_path}/${name}.md
        echo "data_filename: ${name}" >> ${relative_path}/${name}.md
        echo "latest_release_date: ${latest_version_date}" >> ${relative_path}/${name}.md
        echo "---" >> ${relative_path}/${name}.md
    # if exist, check that latest version from feed is the same that the one recorded in md file
    else
        if [ \"${recorded_version_date}\" != ${latest_version_date} ]; then
            echo ${recorded_version_date}
            echo ${latest_version_date}
            # yq modify the original md file and transform it in a non compliant yml file
            # yq w -i ${relative_path}/${name}.md latest_release_date ${latest_version_date}
            ${sed_binary} -i "/latest_release_date\:/c\latest_release_date: ${latest_version_date}" ${relative_path}/${name}.md
        fi
    fi
done
